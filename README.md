### Info

Entry level bookmarking React/Redux web application. 

Few things to have on mind: 
* No any kind of styling included, it looks awful.  
* I made it just to get comfortable with state manipulation using Redux, w/o using any API to store/fetch my data.
* Do with it whatever you want. 

To check how it works clone the repo, navigate into the project folder and run: 
```sh
$ npm install
$ npm run build
```
