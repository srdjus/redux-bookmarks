export function addBookmark(value) {
    return {
        type: "ADD_BOOKMARK", 
        value
    }
}

export function setView(filter) {
    return {
        type: "SET_VIEW", 
        filter
    }
}

