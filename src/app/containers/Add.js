import { connect } from "react-redux"; 
import React from "react";
import { addBookmark } from "../actions";  

class Add extends React.Component {
    handleAdd(e) {
        e.preventDefault();
        let o = {}; 
        if (this.refs.newcat.value)
            o.category = this.refs.newcat.value;  
        else 
            o.category = this.refs.category.value;  
        o.text = this.refs.text.value; 

        this.props.dispatch(addBookmark(o));

        this.refs.newcat.value = this.refs.text.value = "";
        this.refs.text.focus();
    }

    render() {
        return(
            <div>
                <form onSubmit={this.handleAdd.bind(this)}>
                    <input ref="text" type="text" placeholder="link"/>
                    <input ref="newcat" type="text" placeholder="new category"/>
                    <select ref="category">
                        {
                            this.props.categories.map((item, i) => 
                                <option key={i} value={item}>
                                    {item}
                                </option>
                            )
                        }
                    </select>
                    <input type="submit" value="Add"/>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const c = new Set(state.bookmarks.list.map(i =>
     i.category
    ));

    return {
        categories: ["General", ...c]
    }
}

Add = connect(mapStateToProps)(Add); 
export default Add; 