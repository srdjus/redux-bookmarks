import { connect } from "react-redux"; 
import Bookmark from "../components/Bookmark";

const filterBookmarks = (bm, filter) => {
    if (filter === "All")
        return bm; 
    else  
        return bm.filter(i => i.category === filter);
}



const mapStateToProps = (state) => {
    return {
        bookmarks: 
            filterBookmarks(state.bookmarks.list, state.visible.show)
    }
}; 

const GetBookmarks = connect(mapStateToProps)(Bookmark); 
export default GetBookmarks; 