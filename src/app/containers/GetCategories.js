import { connect } from "react-redux"; 
import Category from "../components/Category";
import { setView } from "../actions";

const mapStateToProps = (state) => {
    const c = new Set(state.bookmarks.list.map(i =>
     i.category
    ));

    return {
        categories: ["All", ...c]
    }; 
}

const mapDispatchToProps = (dispatch) => {
    return {    
        setView: (f) => {
            dispatch(setView(f))
        }
    }
}

const GetCategories = connect(mapStateToProps, mapDispatchToProps)(Category); 
export default GetCategories; 

