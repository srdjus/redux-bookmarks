import { createStore, combineReducers } from "redux"; 
import bookmarks from "./reducers/bookmarks";
import visible from "./reducers/visible"; 

const reducers = combineReducers({bookmarks, visible});
let store = createStore(reducers); 

export default store; 