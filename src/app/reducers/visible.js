let initial = {
    show: "All"
}; 

const visible = (state = initial, action) => {
    switch(action.type) {
        case "SET_VIEW": 
            state = {
                ...state, 
                show: action.filter 
            }
            return state; 
        default:  
            return state;
    }
}; 

export default visible; 

