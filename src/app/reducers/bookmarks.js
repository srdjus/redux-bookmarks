let initial = {
    list: []
};

const bookmarks = (state = initial, action) => {
    switch (action.type) {
        case "ADD_BOOKMARK": 
            state = {
                ...state, 
                list: [...state.list, action.value]
            }; 
            return state; 
        default: 
            return state;
    }
};

export default bookmarks;