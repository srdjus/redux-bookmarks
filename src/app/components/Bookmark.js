import React from "react"; 

const Bookmark = ({bookmarks}) => {
    return (
        <div>
            {
                bookmarks.map((item, i) => 
                    <a key={i} href={item.text}>{item.text} </a>
                )
            }
        </div>
    );
}

export default Bookmark; 