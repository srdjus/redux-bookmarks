import React from "react"; 
import Add from "../containers/Add";
import GetCategories from "../containers/GetCategories";
import GetBookmarks from "../containers/GetBookmarks";

const App = () => {
    return( 
        <div>
            <Add />
            <GetCategories />
            <GetBookmarks />
        </div>
    );   
}

export default App; 