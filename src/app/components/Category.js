import React from "react"; 

const Category = ({categories, setView}) => {
    return (
        <div> 
        {
            categories.map((item, i) => 
            <a key={i} href="#" onClick={
                (e) => {
                    e.preventDefault();
                    setView(item);
                } 
            }>
                {item}&nbsp; 
            </a>)
        }
        </div>
    );  
}

export default Category; 