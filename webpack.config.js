var path = require("path"); 

var SRC_DIR = path.resolve(__dirname, "src"); 
var DIST_DIR = path.resolve(__dirname, "dist"); 

module.exports = {
    context: SRC_DIR + "/app", 
    entry: "./index.js", 
    output: {
        path: DIST_DIR + "/app", 
        filename: "bundle.js",
        publicPath: "/app/" 
    }, 

    module: {
        loaders: [
            {
                test: /\.js?/, 
                loader: "babel-loader", 
                include: SRC_DIR, 
                query: {
                    presets: ["es2015", "react", "stage-2"]
                }
            }
        ]
    }
};